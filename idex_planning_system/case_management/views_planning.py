"""
プランニング関連の画面
"""
__author__ = "nemoto"
__date__ = "2020/11/17"


from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView,TemplateView,DetailView,CreateView,UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.db.models import Q


class PlanningIndex(TemplateView):
    """
    プランニングのトップページのページ
    """
    template_name = 'planning/planning_index.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング"

        return ctx


class PlanningSelectClient(TemplateView):
    """
    プランニングのトップページのページ
    """
    template_name = 'planning/CU/planning_select_client.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング > 設定・入力 > クライアント選択"

        return ctx


class PlanningSelectStartDate(TemplateView):
    """
    プランニングのトップページのページ
    """
    template_name = 'planning/CU/planning_select_start_date.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング > 設定・入力 > 開始日入力"

        return ctx


class PlanningSelectMedium(TemplateView):
    """
    プランニングのトップページのページ
    """
    template_name = 'planning/CU/planning_select_medium.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング > 設定・入力 > 媒体選択"

        return ctx


class PlanningTbdFlyerSetting(TemplateView):
    """
    プランニング > 設定・入力 > 広告物仕様仮設定　画面
    """
    template_name = 'planning/CU/planning_tbd_flyer_setting.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング > 設定・入力 > 媒体選択"

        return ctx


class PlanningSelectJudge(TemplateView):
    """
    プランニングのトップページのページ
    """
    template_name = 'planning/CU/planning_select_judge.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング > 設定・入力 > 媒体審査設定"

        return ctx


class PlanningSelectKind(TemplateView):
    """
    プランニングのトップページのページ
    """
    template_name = 'planning/CU/planning_select_kind.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング > 設定・入力 > プラン種別選択"

        return ctx


class PlanningAreaPlanSetting(TemplateView):
    """
    プランニングのトップページのページ
    """
    template_name = 'planning/CU/planning_area_plan_setting.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング > 設定・入力 > プラン設定１（店舗の登録）"

        return ctx


class PlanningDirectPlanSetting(TemplateView):
    """
    プランニングのトップページのページ
    """
    template_name = 'planning/CU/planning_direct_plan_setting.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング > 設定・入力 > プラン設定１（地域の登録）"

        return ctx


class PlanningDetailSetting(TemplateView):
    """
    プランニングのトップページのページ
    """
    template_name = 'planning/CU/planning_detail_setting.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング > 設定・入力 > プラン設定２（国勢調査・消費支出項目の選択）"

        return ctx


class PlanningNameMemo(TemplateView):
    """
    プランニングのトップページのページ
    """
    template_name = 'planning/CU/planning_name_memo.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング > 設定・入力 > プラン設定２（国勢調査・消費支出項目の選択）"

        return ctx


class PlanningSettingConfirm(TemplateView):
    """
    プランニングのトップページのページ
    """
    template_name = 'planning/CU/planning_setting_confirm.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング > 設定・入力 > 確認画面"

        return ctx


class PlanningUnderExe(TemplateView):
    """
    プランニングのトップページのページ
    """
    template_name = 'planning/planning_under_exe.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング > 媒体店舗自動抽出"

        return ctx

