"""
案件管理関連モデルが配置されたモジュール。
"""
__author__ = "nemoto"
__date__ = "2020/11/16"

from django.db import models
from user_management.models import User, UserCompany
from medium_management.models import Medium
from base.model_item import Item
from client_management.models import Client
import datetime
from decimal import Decimal, ROUND_HALF_UP


class Case(models.Model):
    """
    案件モデル
    """
    class Meta:
        """ メタデータ格納クラス """
        verbose_name = "案件"
        db_table = "cases"  # 格納テーブル名

    # 初期設定内容
    name = models.CharField("案件名", max_length=255)
    item = models.ForeignKey(Item, verbose_name="商材", on_delete=models.SET_NULL, null=True)
    client = models.ForeignKey(Client, verbose_name="クライアント", on_delete=models.CASCADE, null=True)
    flyer_num = models.IntegerField("実施予定部数", null=True, blank=True)
    start_date = models.DateField("配布開始日", db_index=True, blank=True, null=True)
    comment = models.TextField("案件備考", blank=True, null=True)
    created_at = models.DateTimeField("作成日時", auto_now_add=True)
    created_by = models.ForeignKey(User, verbose_name="作成者", on_delete=models.PROTECT, related_name="created_by", default=1)
    created_company = models.ForeignKey(UserCompany, verbose_name="作成者企業", related_name="created_company_by", on_delete=models.PROTECT)
    updated_at = models.DateTimeField("更新日時", auto_now=True)
    updated_by = models.ForeignKey(User, verbose_name="更新者", on_delete=models.PROTECT, related_name="updated_by")

    # 受注状況
    ORDER_STATE_CHOICES = (
        (1, 'ヒアリング・提案準備中'),
        (2, '提案済・顧客検討中'),
        (3, '内示有・契約交渉中'),
        (4, '受注承認中'),
        (5, '受注'),
        (11, '配布実施中'),
        (12, '配布完了'),
        (90, '失注'),
        (99, 'その他'),
    )
    order_state = models.IntegerField("進捗", default=1, choices=ORDER_STATE_CHOICES)

    # 使用媒体
    medium = models.ManyToManyField(Medium, verbose_name="使用媒体", through='CaseMedium', null=True)

    # 審査　※チラシについては別class
    JUDGE_STATE_CHOICES = (
        (-1, '審査未表示'),
        (0, '審査前'),
        (1, '審査依頼中'),
        (2, '審査中'),
        (3, '審査NG'),
        (4, '審査OK'),
        (5, '審査不要'),
    )
    judge_state = models.IntegerField("審査状態", default=-1, choices=JUDGE_STATE_CHOICES)
    judge_request_datetime = models.DateTimeField("最新の審査依頼日時", blank=True, null=True)
    judge_request_comment = models.TextField("審査依頼に関するコメント", blank=True, null=True)
    judge_comment = models.TextField("審査に関するコメント", blank=True, null=True)

    # プランニング
    PLAN_CODE_CHOICES = (
        (1, 'エリアマーケティング型'),
        (2, 'ダイレクトマーケティング型'),
        (3, '店舗指定型'),
        (4, '未決定'),
    )
    plan_code = models.IntegerField("配布種別", default=4, choices=PLAN_CODE_CHOICES)
    planning_request_flg = models.BooleanField("プランニング依頼フラグ", default=False)

    # 店舗確保
    POST_STATE_CHOICES = (
        (-1, '店舗確保画面未表示'),
        (0, '未確保'),
        (1, '店舗確保申請中'),
        (2, '店舗確保却下'),
        (3, '店舗確保承認済み'),
        (9, 'その他'),
    )
    post_state = models.IntegerField("店舗確保進捗", default=-1, choices=POST_STATE_CHOICES)
    post_application_flg = models.BooleanField("転記内容申請フラグ", default=False)  # 申請するとTrue
    post_application_by = models.ForeignKey(User, verbose_name="転記申請者", on_delete=models.PROTECT, null=True, blank=True,
                                            related_name="post_application_by")
    post_application_date_time = models.DateTimeField("転記内容申請日時", null=True, blank=True)
    post_approval_flg = models.BooleanField("転記承認フラグ", default=False)  # 承認されるとTrue
    post_approval_by = models.ForeignKey(User, verbose_name="転記承認者", on_delete=models.PROTECT, null=True, blank=True,
                                         related_name="post_approval_by")
    post_approval_date_time = models.DateTimeField("承認済日時", null=True, blank=True)
    post_rejection_flg = models.BooleanField("転記却下フラグ", default=False)  # 却下されるとTrue
    post_rejection_by = models.ForeignKey(User, verbose_name="転記却下者", on_delete=models.PROTECT, null=True, blank=True,
                                          related_name="post_reject_by")
    post_rejection_date_time = models.DateTimeField("却下日時", null=True, blank=True)

    # 申込
    case_no = models.CharField("案件番号", max_length=255, blank=True, null=True)
    APPLY_STATE_CHOICES = (
        (-1, '申込画面未表示'),
        (0, '申込前'),
        (1, '申込中'),
        (2, '申込承認済'),
        (90, '申込却下'),
        (99, 'その他'),
    )
    apply_state = models.IntegerField("申込状態", default=-1, choices=APPLY_STATE_CHOICES)
    apply_date_time = models.DateTimeField("申込申請日時", null=True, blank=True)
    apply_by = models.ForeignKey(User, verbose_name="申込申請者", on_delete=models.PROTECT, null=True, blank=True,
                                 related_name="apply_by")
    apply_approval_by = models.ForeignKey(User, verbose_name="申込承認者", on_delete=models.PROTECT, null=True, blank=True,
                                          related_name="apply_approval_by")
    apply_approval_date_time = models.DateTimeField("申込み承認済日時", null=True, blank=True)
    apply_rejection_by = models.ForeignKey(User, verbose_name="申込却下者", on_delete=models.PROTECT, null=True, blank=True,
                                           related_name="apply_reject_by")
    apply_rejection_date_time = models.DateTimeField("申込却下日時", null=True, blank=True)
    apply_rejection_reason = models.TextField("申込却下理由", blank=True, null=True)
    apply_cancel_reason = models.TextField("申込キャンセル理由", blank=True, null=True)

    # 部材入庫
    WAREHOUSING_STATE_CHOICES = (
        (-1, '入庫先調整画面未表示'),
        (0, '入庫先調整前'),
        (1, '入庫先調整中'),
        (2, '入庫先割当完了'),
        (99, '入庫先割当が特殊なもの、その他'),
    )
    warehousing_state = models.IntegerField("入庫先割当状態", default=-1, choices=WAREHOUSING_STATE_CHOICES)
    carry_in_request_date_st = models.DateField("部材入庫依頼日（開始）",null=True,blank=True)
    carry_in_request_date_end = models.DateField("部材入庫依頼日（終了）",null=True,blank=True)
    carry_in_decided_date = models.DateField("部材入庫日",null=True,blank=True)
    warehousing_text = models.TextField("入庫想定先", null=True, blank=True)

    # 反響
    reaction_text = models.TextField("反響",null=True,blank=True)

    def __str__(self):
        return str(self.name)


class CaseMedium(models.Model):
    """
    案件における媒体審査、配布アナウンス、売上・受注等に利用するモデル。
    """
    class Meta:
        """ メタデータ格納クラス """
        db_table = "cases_media"  # 格納テーブル名
        unique_together = (("case", "medium"),)
        ordering = ['case', 'medium']  # オブジェクト取得の際のデフォルトの並び順

    case = models.ForeignKey(Case, verbose_name="案件", on_delete=models.CASCADE)
    medium = models.ForeignKey(Medium, verbose_name="媒体", on_delete=models.CASCADE)
    created_at = models.DateTimeField("作成日時", auto_now_add=True)

    """
    # 審査関連
    JUDGE_STATE_CHOICES = (
        (0, '審査前'),
        (1, '審査依頼中'),
        (2, '審査中'),
        (3, '審査NG'),
        (4, '審査OK'),
        (5, '審査不要'),
    )
    judge_state = models.IntegerField("媒体別審査状態", default=0, choices=JUDGE_STATE_CHOICES)
    not_use_flg = models.BooleanField("審査NG了承フラグ", default=False)

    # 配布アナウンス
    announce_flg = models.BooleanField("配布アナウンス完了フラグ", default=False)  # 完了するとTrue

    # 本案件における媒体費
    medium_sales = models.FloatField("本案件における媒体費", default=6.5)
    medium_flyer_num = models.IntegerField("本案件における媒体合計部数", default=0)
    """
