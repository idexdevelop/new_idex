import os
from django.conf.urls import url
from . import views_planning,views_plan_result

app_name = os.path.split(os.path.dirname(os.path.abspath(__file__)))[-1]  # app名 = dir名

urlpatterns = [
    url(r'planning_index/$', views_planning.PlanningIndex.as_view(), name='planning_index'),

    # プラン作成・編集View
    url(r'planning_select_client/$', views_planning.PlanningSelectClient.as_view(), name='planning_select_client'),
    url(r'planning_select_start_date/$', views_planning.PlanningSelectStartDate.as_view(), name='planning_select_start_date'),
    url(r'planning_select_medium/$', views_planning.PlanningSelectMedium.as_view(), name='planning_select_medium'),
    url(r'planning_tbd_flyer_setting/$', views_planning.PlanningTbdFlyerSetting.as_view(), name='planning_tbd_flyer_setting'),
    url(r'planning_select_judge/$', views_planning.PlanningSelectJudge.as_view(), name='planning_select_judge'),
    url(r'planning_select_kind/$', views_planning.PlanningSelectKind.as_view(), name='planning_select_kind'),
    url(r'planning_area_plan_setting/$', views_planning.PlanningAreaPlanSetting.as_view(), name='planning_area_plan_setting'),
    url(r'planning_direct_plan_setting/$', views_planning.PlanningDirectPlanSetting.as_view(), name='planning_direct_plan_setting'),
    url(r'planning_detail_setting/$', views_planning.PlanningDetailSetting.as_view(), name='planning_detail_setting'),
    url(r'planning_name_memo/$', views_planning.PlanningNameMemo.as_view(), name='planning_name_memo'),
    url(r'planning_setting_confirm/$', views_planning.PlanningSettingConfirm.as_view(), name='planning_setting_confirm'),
    url(r'planning_under_exe/$', views_planning.PlanningUnderExe.as_view(), name='planning_under_exe'),

    # プラン作成・編集function

    # プラン結果表示・調整
    url(r'plan_result/$', views_plan_result.PlanResult.as_view(), name='plan_result'),

]
