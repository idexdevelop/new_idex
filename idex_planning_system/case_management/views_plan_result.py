"""
プランニング関連の画面
"""
__author__ = "nemoto"
__date__ = "2020/11/17"


from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView,TemplateView,DetailView,CreateView,UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.db.models import Q


class PlanResult(TemplateView):
    """
    プランニングのトップページのページ
    """
    template_name = 'planning/result/plan_result.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "プランニング > 結果表示"

        return ctx


