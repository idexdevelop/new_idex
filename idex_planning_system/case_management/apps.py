from django.apps import AppConfig


class CaseManagementConfig(AppConfig):
    name = 'case_management'
