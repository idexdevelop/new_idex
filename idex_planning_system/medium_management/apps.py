from django.apps import AppConfig


class MediumManagementConfig(AppConfig):
    name = 'medium_management'
