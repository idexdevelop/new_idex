"""
媒体関連モデル配置モジュール
"""
__author__ = "nemoto"
__date__ = "2020/11/16"

from django.db import models
from user_management.models import User, UserCompany
from base.model_prefecture import Prefecture
from base.model_ng_type import NGType


class MediumCategory(models.Model):
    """
    媒体カテゴリ情報クラス
    """
    class Meta:
        """ メタデータ格納クラス """
        verbose_name = "媒体カテゴリ"
        db_table = "medium_categories"  # 格納テーブル名
        ordering = ['sort_num']  # オブジェクト取得の際のデフォルトの並び順

    name = models.CharField("カテゴリ名", max_length=255, unique=True)
    category_code = models.IntegerField("カテゴリーコード", blank=True, null=True, unique=True) # 2桁を想定 10 ~ 99
    sort_num = models.IntegerField("ソート順", unique=True)
    logo = models.ImageField("媒体カテゴリロゴ", upload_to='medium_category_logo', blank=True, null=True)
    updated_at = models.DateTimeField("更新日時", auto_now=True)
    updated_by = models.ForeignKey(User, verbose_name="更新者", on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class Medium(models.Model):
    """
    媒体情報クラス
    広告配布媒体の社名やブランド名を格納
    """
    class Meta:
        """ メタデータ格納クラス """
        verbose_name = "媒体"
        db_table = "media"  # 格納テーブル名
        ordering = ['code']  # オブジェクト取得の際のデフォルトの並び順

    code = models.CharField("媒体コード", max_length=4, unique=True)  # 媒体コード2桁 + 媒体枝番1桁
    name = models.CharField("表示媒体名", max_length=255, unique=True)  # e.g.) ダイソー
    company = models.CharField("媒体社正式名", max_length=255)  # e.g.) 株式会社ダイソー産業
    category = models.ForeignKey(MediumCategory, verbose_name="カテゴリ", on_delete=models.CASCADE)
    is_active = models.BooleanField("使用可能", default=True)
    logo = models.ImageField("媒体ロゴ", upload_to='medium_logo')
    distribute_period = models.IntegerField("配布日数", default=7)
    block_period = models.IntegerField("ブロック日数", default=7)
    flyer_num_lower_limit = models.IntegerField("案件ごとの最低配布部数", default=0)
    two_thousand_flg = models.BooleanField("2000部単位制約", default=False)

    available_companies = models.ManyToManyField(UserCompany, verbose_name="使用可能ユーザー企業")
    updated_at = models.DateTimeField("更新日時", auto_now=True)
    updated_by = models.ForeignKey(User, verbose_name="更新者", on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class MediumShop(models.Model):
    """
    媒体個別店情報クラス
    """
    class Meta:
        """ メタデータ格納クラス """
        verbose_name = "媒体個別店"
        db_table = "medium_shops"  # 格納テーブル名
        unique_together = (("medium", "shop_code"),)  # 複合ユニーク制約
        ordering = ['medium', 'shop_code']  # オブジェクト取得の際のデフォルトの並び順

    is_active = models.BooleanField("使用可能", default=True)
    updated_at = models.DateTimeField("更新日時", auto_now=True)
    updated_by = models.ForeignKey(User, verbose_name="更新者", on_delete=models.PROTECT)

    # 必須
    medium = models.ForeignKey(Medium, verbose_name="媒体", on_delete=models.CASCADE)
    shop_code = models.CharField("店舗コード",max_length=10)
    master_code = models.CharField("マスターコード", max_length=15, unique=True)
    name = models.CharField("個別店舗名", max_length=100)
    abb_name = models.CharField("店舗略称", max_length=100)
    address = models.CharField("住所", max_length=255)
    post_code = models.CharField("郵便番号", max_length=20)  # ハイフン込み
    prefecture = models.ForeignKey(Prefecture, verbose_name="都道府県", on_delete=models.PROTECT,
                                   db_column="prefecture_code")
    trade_area = models.IntegerField("商圏(km)", default=5)
    longitude = models.DecimalField("経度(X)", max_digits=12, decimal_places=6)
    latitude = models.DecimalField("緯度(Y)", max_digits=12, decimal_places=6)

    # 準必須
    distribute = models.IntegerField("配布可能部数", default=0)
    open_date = models.DateField("オープン日", default='1900-01-01')
    close_date = models.DateField("閉店日", null=True, blank=True)
    renewal_open_date = models.DateField("リニューアルオープン日", null=True, blank=True)
    renewal_close_date = models.DateField("リニューアル閉店日", null=True, blank=True)

    ng = models.ManyToManyField(NGType, verbose_name="NG項目", through='ShopNg', null=True, blank=True) #NG項目

    # ラベル
    address_building = models.CharField("住所(建物名)", max_length=255, blank=True)
    municipality = models.CharField("市町区村", max_length=255, blank=True)

    tel = models.CharField("TEL", max_length=255, blank=True)
    fax = models.CharField("FAX", max_length=255, blank=True)

    not_active_reason = models.CharField("使用不可の理由", max_length=255, blank=True)

    comment = models.TextField("備考", blank=True)

    def __str__(self):
        return self.name


class ShopNg(models.Model):
    """
    媒体個別店とNG項目を結びつける為のモデル
    """
    class Meta:
        """ メタデータ格納クラス """
        db_table = "shop_ng"  # 格納テーブル名
        auto_created = True

    medium_shop = models.ForeignKey(MediumShop, verbose_name="媒体個別店", on_delete=models.SET_NULL,null=True)
    ng = models.ForeignKey(NGType, verbose_name="NG", on_delete=models.SET_NULL,null=True)
    created_at = models.DateTimeField("作成日時", auto_now_add=True)

