from django.contrib import admin
from .models import MediumCategory,MediumShop,Medium,ShopNg


@admin.register(MediumCategory)
class MediumCategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Medium)
class MediumAdmin(admin.ModelAdmin):
    pass


@admin.register(MediumShop)
class MediumShopAdmin(admin.ModelAdmin):
    search_fields = ('master_code',)


@admin.register(ShopNg)
class ShopNgAdmin(admin.ModelAdmin):
    pass
