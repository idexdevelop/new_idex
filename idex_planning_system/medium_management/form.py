"""
媒体関連モデルフォーム用モジュール
"""
__author__ = "KeitaNemoto"
__date__ = "2019/03/01"


from django import forms
from medium_management.models import MediumCategory,Medium,MediumShop

class MediumCategoryForm(forms.ModelForm):
    """媒体カテゴリ用フォーム"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            if isinstance(field, forms.fields.ImageField):
                continue
            else:
                field.widget.attrs["class"] = "form-control"

    class Meta:
        model = MediumCategory
        fields = ["name", "sort_num"]


class MediumForm(forms.ModelForm):
    """媒体用フォーム"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            if isinstance(field, forms.fields.ImageField):
                continue
            else:
                field.widget.attrs["class"] = "form-control"

    class Meta:
        model = Medium
        fields = ["category", "code", "name", "company", "logo", "distribute_period", "block_period",
                  "multi_bags_block_period","flyer_num_lower_limit", "two_thousand_flg", "available_companies",
                  "available_insurance_planning", "insurance_planning_kind", "is_active",
                  "special_label_sticker_flg", "label_sticker_header", "label_sticker_text", "label_sticker_text_height",
                  "label_sticker_red_text", "label_sticker_red_text_height", "label_sticker_contact",
                  "store_guide_header", "store_guide_text"]


class MediumShopForm(forms.ModelForm):
    """媒体個別店用フォーム"""

    is_active = forms.BooleanField(
        label='使用可能（※使用不可にする場合は、このページからではなく、詳細ページから「削除」ボタンをクリックすること）',
        required=False,
    )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.is_active = kwargs.pop('is_active', None)
        super().__init__(*args, **kwargs)

        if not self.user.user_company.group.name == "イデックス業務管理" or self.is_active:
            del self.fields['is_active']

        for field in self.fields.values():
            if isinstance(field, forms.fields.ImageField):
                continue
            else:
                field.widget.attrs["class"] = "form-control"

    class Meta:
        model = MediumShop
        fields = ["is_active","medium","shop_code","medium_shop_code","master_code","old_master_code","business_category","name","abb_name",
                  "post_code","prefecture","municipality","address","address_building",
                  "trade_area","longitude","latitude","tel","fax",
                  "bag_a_distribute","bag_b_distribute",
                  "open_date","close_date","renewal_open_date","renewal_close_date",
                  "other1_distribute","other2_distribute","two_week_shop_inclusion",
                  "tsubo_num","consumer_num","sales","not_active_reason","comment","comment2","ng",]
