"""プロジェクト全体で使用するモデル、関数、変数などを格納するアプリケーション"""

from django.apps import AppConfig


class BaseConfig(AppConfig):
    name = 'base'
