"""
商材モデルが配置されたモジュール。
"""
__author__ = "nemoto"
__date__ = "2020/11/16"

from django.db import models
from user_management.models import User


class Item(models.Model):
    """
    商材情報クラス
    """
    class Meta:
        """ メタデータ格納クラス """
        verbose_name = "商材"
        db_table = "items"  # 格納テーブル名
        ordering = ['sort_num']  # オブジェクト取得の際のデフォルトの並び順

    name = models.CharField("商材名", max_length=255, unique=True)
    sort_num = models.IntegerField("ソート順", unique=True)
    created_at = models.DateTimeField("作成日時", auto_now_add=True)
    updated_at = models.DateTimeField("更新日時", auto_now=True)
    updated_by = models.ForeignKey(User, verbose_name="更新者", on_delete=models.PROTECT)

    def __str__(self):
        return self.name
