"""
アプリ内で共通に使われるモデルが配置されたモジュール。
"""
__author__ = "nemoto"
__date__ = "2020/11/16"

from django.db import models

from base.model_ng_type import NGType
from base.model_prefecture import Prefecture
from base.model_survey import Survey
from base.model_item import Item
from user_management.models import User

