from django.contrib import admin
from .model_survey import Survey
from .model_prefecture import Prefecture
from .model_ng_type import NGType

# Register your models here.

@admin.register(Survey)
class SurveyAdmin(admin.ModelAdmin):
    pass


@admin.register(Prefecture)
class PrefectureAdmin(admin.ModelAdmin):
    pass


@admin.register(NGType)
class NGTypeAdmin(admin.ModelAdmin):
    pass
