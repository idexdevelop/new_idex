"""
アプリ内で共通に使われる都道府県情報モデルが配置されたモジュール。
"""
__author__ = "nemoto"
__date__ = "2020/11/16"

from django.db import models


class Prefecture(models.Model):
    """
    都道府県情報モデル
    """
    class Meta:
        """ メタデータ格納クラス """
        db_table = "prefectures"  # 格納テーブル名
        ordering = ['code']  # オブジェクト取得の際のデフォルトの並び順

    code = models.IntegerField("都道府県コード", primary_key=True)
    name = models.CharField("都道府県名", max_length=12, unique=True)

    REGION_CHOICE = (
        (0, '北海道・東北地方'),
        (1, '関東地方'),
        (2, '中部地方'),
        (3, '近畿地方'),
        (4, '中国・四国地方'),
        (5, '九州・沖縄地方'),
    )
    region = models.IntegerField("地方名", choices=REGION_CHOICE)

    def __str__(self):
        return self.name
