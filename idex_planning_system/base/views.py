from django.views.generic import TemplateView



class DashBoard(TemplateView):
    """ダッシュボード"""
    template_name = 'dash_board.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = "IDEX PLANNING SYSTEM"
        return ctx