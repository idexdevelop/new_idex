"""
自作テンプレート
Django template中でindexを使えるようにする
"""
from django import template
from typing import Union
register = template.Library()


@register.filter
def index(list_or_dict: Union[list, dict], index: int):
    """
    dictかlistでindex に対応する要素を返す
    """
    return list_or_dict[index]
