"""
自作テンプレート
Django template中でgetatterを使えるようにする
https://djangosnippets.org/snippets/38/
"""
from django import template
register = template.Library()


@register.filter
def getattr(obj: object, attribute: str) -> object:
    """
    Try to get an attribute from an object.
    Example: {% if block|getattr:editable %}
    """
    try:
        return obj.__getattribute__(attribute)
    except AttributeError:
        return obj.__dict__[attribute]
