"""
自作テンプレート
Django template中でfieldにclassを追加するaddclasstofieldを使えるようにする
"""
from django import template
from django.forms import Field
register = template.Library()


@register.filter
def addclasstofield(field: Field, css: str):
    """ fieldにclassを追加する """
    return field.as_widget(attrs={"class": css})
