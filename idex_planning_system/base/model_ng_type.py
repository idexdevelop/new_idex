"""
アプリ内で共通に使われるNG項目モデルが配置されたモジュール。
"""
__author__ = "nemoto"
__date__ = "2020/11/16"

from django.db import models


class NGType(models.Model):
    """
    NG項目情報モデル
    """
    class Meta:
        """ メタデータ格納クラス """
        verbose_name = "NG項目"
        db_table = "ng_types"  # 格納テーブル名
        ordering = ['id']  # オブジェクト取得の際のデフォルトの並び順

    name = models.CharField("NG項目名", max_length=30, unique=True)
    verbose_name = models.CharField("NG項目バーボス名", max_length=30, unique=True)

    def __str__(self):
        return self.verbose_name
