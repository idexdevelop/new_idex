"""
調査項目モデルが配置されたモジュール。
"""
__author__ = "nemoto"
__date__ = "2020/11/16"

from django.db import models


class Survey(models.Model):
    """
    調査項目モデル
    """
    class Meta:
        """ メタデータ格納クラス """
        verbose_name = "調査項目"
        db_table = "surveys"  # 格納テーブル名
        ordering = ['id']

    SURVEY_CATEGORY_CHOICE = (
        (0, '国勢調査項目'),
        (1, '消費支出項目'),
    )
    survey_category = models.IntegerField("調査カテゴリ", choices=SURVEY_CATEGORY_CHOICE, default=0)
    name = models.CharField("調査項目名", max_length=80, unique=True)  # MediumShopのフィールド名
    verbose_name = models.CharField("調査項目バーボス名", max_length=50, unique=True)  # MediumShopのフィールドverbose名
    updated_at = models.DateTimeField("更新日時", auto_now=True)

    def __str__(self):
        return self.verbose_name
