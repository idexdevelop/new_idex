import os
from . import views
from django.conf.urls import url

app_name = os.path.split(os.path.dirname(os.path.abspath(__file__)))[-1]  # app名 = dir名

urlpatterns = [
    url(r'$', views.DashBoard.as_view(), name='dash_board'),
]
