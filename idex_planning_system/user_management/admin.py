from django.contrib import admin
from user_management.models import User, UserCompany


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['username', 'last_name', 'first_name', 'email']
    pass


@admin.register(UserCompany)
class UserCompanyAdmin(admin.ModelAdmin):
    pass

