"""
ユーザー関連モデル配置モジュール
"""
__author__ = "nemoto"
__date__ = "2020/10/16"

from django.db import models
from django.contrib.auth.models import AbstractUser, Group


class UserCompany(models.Model):
    """ユーザー所属企業情報クラス"""
    class Meta:
        """メタデータ格納クラス"""
        verbose_name = "ユーザー所属企業"
        db_table = "user_companies"  # 格納テーブル名

    name = models.CharField("企業名", max_length=255, unique=True)
    group = models.ForeignKey(Group, verbose_name="グループ", on_delete=models.PROTECT)
    is_active = models.BooleanField("使用可能", default=True)
    updated_at = models.DateTimeField("更新日時", auto_now=True)

    def __str__(self):
        return self.name

class User(AbstractUser):
    """認証用ユーザークラス"""
    class Meta:
        """ メタデータ格納クラス """
        verbose_name = "ユーザー"
        db_table = "auth_user"  # 格納テーブル名

    user_company = models.ForeignKey(UserCompany, on_delete=models.PROTECT, verbose_name="所属企業",
                                db_column="user_company_id", null=True)
    idex_ope_user_flg = models.BooleanField("イデックスの業務ユーザーフラグ", default=False)
    kiyaku_check = models.BooleanField("規約チェックフラグ", default=False)

    def __str__(self):
        return self.last_name + " " + self.first_name
