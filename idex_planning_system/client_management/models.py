"""
広告主(クライアント)関連モデルが配置されたモジュール。
"""
__author__ = "nemoto"
__date__ = "2018/5/15"

from django.db import models
from user_management.models import User, UserCompany
from base.model_item import Item


class Client(models.Model):
    """
    広告主情報モデル
    """
    class Meta:
        """ メタデータ格納クラス """
        verbose_name = "クライアント"
        db_table = "clients"  # 格納テーブル名
        ordering = ['id']  # オブジェクト取得の際のデフォルトの並び順
        unique_together = (("name", "created_company"),("official_name", "created_company"),)  # 複合ユニーク制約

    name = models.CharField("表示用のクライアント（企業）名", max_length=255)  # e.g.) すかいらーく
    official_name = models.CharField("クライアント（企業）正式名称", max_length=255)  # e.g.) 株式会社すかいらーく
    kana = models.CharField("クライアント名ふりがな", max_length=255, blank=True)
    url = models.CharField("URL", max_length=255, blank=True)
    logo = models.ImageField("クライアントのロゴ", upload_to="client_logo", null=True, blank=True)
    comment = models.TextField("フランチャイズ情報等の備考", blank=True, null=True)
    item = models.ForeignKey(Item, verbose_name="広告物の標準的な商材", on_delete=models.PROTECT, default=1)
    created_at = models.DateTimeField("作成日時", auto_now_add=True)
    updated_at = models.DateTimeField("更新日時", auto_now=True)
    updated_by = models.ForeignKey(User, verbose_name="更新者", on_delete=models.PROTECT)
    created_company = models.ForeignKey(UserCompany, verbose_name="作成者企業", on_delete=models.PROTECT)
    latest_use_at = models.DateTimeField("最新の案件作成日時", blank=True, null=True)

    def __str__(self):
        return self.name







