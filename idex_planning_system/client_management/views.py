from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Client

# Create your views here.

class ClientIndex(LoginRequiredMixin, TemplateView):
    """
    クライアントを管理するインデックスのページ。
    最新の更新状況についても表示される画面。
    <form>タグを使用しないため、TemplateViewを利用する。

    """
    template_name = 'client_management/client_index.html'

    def get_context_data(self, **kwargs):
        """ template html に渡すcontextの定義 """
        context = super().get_context_data(**kwargs)
        context['title'] = context['title'] = "クライアント管理"
        context['clients'] = Client.objects.filter(updated_by=self.request.user,is_active=True).order_by('-updated_at')[0:14]

        return context
